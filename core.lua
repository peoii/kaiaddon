-- Slash Commands
------------------------------------------------------------------
SlashCmdList['FRAME'] = function() ChatFrame1:AddMessage(GetMouseFocus():GetName() or '(Unnamed frame)') end
SLASH_FRAME1 = '/frame'

SlashCmdList['RELOAD'] = function() ReloadUI() end
SLASH_RELOAD1 = '/rl'

SlashCmdList["TICKET"] = function() ToggleHelpFrame() end
SLASH_TICKET1 = "/ticket"
SLASH_TICKET2 = "/gm"

SlashCmdList["GETPARENT"] = function() print(GetMouseFocus():GetParent():GetName()) end
SLASH_GETPARENT1 = "/parent"

SlashCmdList["RCSLASH"] = function() DoReadyCheck() end
SLASH_RCSLASH1 = "/rc"

SlashCmdList["CHECKROLE"] = function() InitiateRolePoll() end
SLASH_CHECKROLE1 = '/cr'

SlashCmdList["CTPSLASH"] = function() ConvertToParty() end
SLASH_CTPSLASH1 = "/ctp"

SlashCmdList["CTRSLASH"] = function() ConvertToRaid() end
SLASH_CTRSLASH1 = "/ctr"

SlashCmdList["SPECSLASH"] = function() if GetActiveSpecGroup()==1 then SetActiveSpecGroup(2) elseif GetActiveSpecGroup()==2 then SetActiveSpecGroup(1) end end
SLASH_SPECSLASH1 = "/spec"

SlashCmdList["RELEASESLASH"] = function() RepopMe() end
SLASH_CTRSLASH1 = "/release"


-- Hiding the Raidframe of Blizzards RaidUI (the bar on the left top)
------------------------------------------------------------------
--CompactRaidFrameManager:UnregisterAllEvents() 
--CompactRaidFrameManager:Hide() 
--CompactRaidFrameContainer:UnregisterAllEvents() 
--CompactRaidFrameContainer:Hide()

-- Camera and SS quality
------------------------------------------------------------------
s = C_CVar.SetCVar("screenshotQuality", 10)
s = C_CVar.SetCVar("cameraDistanceMaxZoomFactor", 2.6)
s = C_CVar.SetCVar("CameraReduceUnexpectedMovement", 1)
s = C_CVar.SetCVar("autoLootDefault", 1)
s = C_CVar.SetCVar("profanityFilter", 0)
s = C_CVar.SetCVar("lfgSelectedRoles", 0)
s = C_CVar.SetCVar("pvpSelectedRoles", 0)
s = C_CVar.SetCVar("whisperMode", "inline")
s = C_CVar.SetCVar("chatBubbles", 1)
s = C_CVar.SetCVar("chatBubblesParty", 1)
s = C_CVar.SetCVar("worldPreloadNonCritical", 0)
s = C_CVar.SetCVar("GxAllowCachelessShaderMode", 0)
s = C_CVar.SetCVar("ResampleAlwaysSharpen", 1)
s = C_CVar.SetCVar("camerafov",90)
s = C_CVar.SetCVar("TargetNearestUseOld", 1)
--s = C_CVar.SetCVar("", )
--s = C_CVar.SetCVar("", )
